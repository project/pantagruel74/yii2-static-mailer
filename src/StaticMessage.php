<?php

namespace Pantagruel74\Yii2StaticMailer;

use yii\mail\BaseMessage;
use yii\mail\MessageInterface;

class StaticMessage extends BaseMessage
{
    protected string $charset = 'utf-8';
    protected $from = '';
    protected $to = '';
    protected $replyTo = '';
    protected $cc = '';
    protected $bcc = '';
    protected string $subject = '';
    public string $body = '';
    public array $files = [];

    /**
     * @return string
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * @param string $charset
     * @return $this
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
        return $this;
    }

    /**
     * @return array|string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param array|string $from
     * @return $this
     */
    public function setFrom($from)
    {
        $this->from = $from;
        return $this;
    }

    /**
     * @return array|string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param array|string $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return array|string
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @param $replyTo
     * @return $this
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;
        return $this;
    }

    /**
     * @return array|string
     */
    public function getCc()
    {
        return $this->cc;
    }

    /***
     * @param array|string $cc
     * @return $this
     */
    public function setCc($cc)
    {
        $this->cc = $cc;
        return $this;
    }

    /**
     * @return array|string
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @param array|string $bcc
     * @return $this
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setTextBody($text)
    {
        $this->body = $text;
        return $this;
    }

    /**
     * @param string $html
     * @return $this
     */
    public function setHtmlBody($html)
    {
        $this->body = $html;
        return $this;
    }

    /**
     * @param $fileName
     * @param array $options
     * @return $this
     */
    public function attach($fileName, array $options = [])
    {
        $this->files[] = $fileName;
        return $this;
    }

    /**
     * @param $content
     * @param array $options
     * @return $this
     */
    public function attachContent($content, array $options = [])
    {
        $this->files[] = 'attached';
        return $this;
    }

    /**
     * @param string $fileName
     * @param array $options
     * @return string|void
     */
    public function embed($fileName, array $options = [])
    {
        $this->files[] = $fileName;
        return $fileName;
    }

    /**
     * @param $content
     * @param array $options
     * @return string
     */
    public function embedContent($content, array $options = [])
    {
        $this->files[] = 'attached';
        return 'attached';
    }

    /**
     * @return bool|string
     */
    public function toString()
    {
        return print_r($this, true);
    }

    /**
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @return array
     */
    public function getFiles(): array
    {
        return $this->files;
    }
}