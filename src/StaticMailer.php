<?php

namespace Pantagruel74\Yii2StaticMailer;

use yii\mail\BaseMailer;
use yii\mail\MessageInterface;

class StaticMailer extends BaseMailer
{
    public static array $messages = [];
    public $messageClass = 'Pantagruel74\Yii2StaticMailer\StaticMessage';

    /**
     * @param $message
     * @return bool
     */
    protected function sendMessage($message)
    {
        self::$messages[] = $message;
        return true;
    }
}